package com.dy.service;

import java.util.List;

import com.dy.base.model.Page;
import com.dy.base.model.Pageable;
import com.dy.model.User;

public interface IUserService {
	public void saveUser(User user);
	
	public List<User> queryUsers();
	
	public Page<User> queryPageStus(Pageable pageable);
}
