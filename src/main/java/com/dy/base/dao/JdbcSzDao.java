package com.dy.base.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

@Repository
//@Scope("prototype")   
public class JdbcSzDao<T> {
	
	protected JdbcTemplate jdbcTemplate;
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public SimpleJdbcCall getJdbcCall() {
		return jdbcCall;
	}
	protected SimpleJdbcCall jdbcCall;
	
	private DataSource dataSourceTo;

	public DataSource getDataSourceTo() {
		return dataSourceTo;
	}
	@Autowired
	@Resource(name="dataSourceTo")
	public void setDataSourceTo(DataSource dataSourceTo) {
		this.dataSourceTo = dataSourceTo;
		jdbcTemplate = new JdbcTemplate(dataSourceTo);
		jdbcCall = new SimpleJdbcCall(dataSourceTo);
	}

}


