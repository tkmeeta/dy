package com.dy.dao;

import java.util.List;

import com.dy.base.model.Page;
import com.dy.base.model.Pageable;
import com.dy.model.User;

public interface IUserDao {
	public User queryUser(int id);
	public void saveUser(User user);
	public List<User> queryUsers();
	public Page<User> queryUsersByPage(Pageable pageable);
}
