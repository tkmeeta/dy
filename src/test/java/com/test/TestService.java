package com.test;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.dy.base.model.Page;
import com.dy.base.model.PageRequest;
import com.dy.model.Stu;
import com.dy.model.User;
import com.dy.service.IStuService;
import com.dy.service.IUserService;

@TransactionConfiguration(defaultRollback=false)
public class TestService extends BaseContextTest{
	@Autowired
	private IStuService stuService;

	@Autowired
	private IUserService userService;

	@Test
	public void testStu(){
//		Stu stu = new Stu("stu3");
//		stu.setId(3);
//		stuService.saveStu(stu);
		List<Stu> list = stuService.queryStus();
		System.out.println(list);
		Stu stu = stuService.queryStu(1);
		System.out.println(stu);
		List<User> list2 = userService.queryUsers();
		System.out.println(list2);
		PageRequest pageable = new PageRequest(1,3);
		Page<User> userPages = userService.queryPageStus(pageable);
		System.out.println(userPages.getContent().size());
		System.out.println(userPages);
		
		Page<Stu> stuPages = stuService.queryPageStus(pageable);
		System.out.println(stuPages.getContent().size());
		System.out.println(stuPages);
	}
	
//	@Test
//	public void testUser(){
//		List<User> list = userService.queryUsers();
//		System.out.println(list.size());
//		System.out.println(list);
//	}
}
