package com.dy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dy.base.model.Page;
import com.dy.base.model.Pageable;
import com.dy.dao.IStuDao;
import com.dy.dao.IUserDao;
import com.dy.model.Stu;
import com.dy.model.User;
import com.dy.service.IStuService;

@Service
@Transactional(readOnly = true)
public class StuServiceImpl implements IStuService{

	@Autowired
	private IStuDao stuDao;
	
	@Autowired
	private IUserDao userDao;
	
	@Override
	@Transactional(readOnly=false)
	public void saveStu(Stu stu){
		stuDao.saveStu(stu);
		User user = new User("user12");
		user.setId(12);
		userDao.saveUser(user);
	}
	
	@Override
	public Stu queryStu(int id) {
		return stuDao.queryStudent(id);
	}
	
	@Override
	public List<Stu> queryStus() {
		List<Stu> list = stuDao.queryStudents();
		return list;
	}

	@Override
	public Page<Stu> queryPageStus(Pageable pageable) {
		Page<Stu> page = stuDao.queryStuByPage(pageable);
		return page;
	}
	
}
