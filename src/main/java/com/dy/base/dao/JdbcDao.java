package com.dy.base.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

@Repository
//@Scope("prototype")   
public class JdbcDao<T>{
	protected JdbcTemplate jdbcTemplate;
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public SimpleJdbcCall getJdbcCall() {
		return jdbcCall;
	}
	protected SimpleJdbcCall jdbcCall;
	
	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}
	
	@Autowired
	@Resource(name="dataSource")
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcCall = new SimpleJdbcCall(dataSource);
	}

}


