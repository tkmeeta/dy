package com.dy.dao;

import java.util.List;

import com.dy.base.model.Page;
import com.dy.base.model.Pageable;
import com.dy.model.Stu;

public interface IStuDao {
	public void saveStu(Stu stu);
	public Stu queryStudent(int id);
	public List<Stu> queryStudents();
	public Page<Stu> queryStuByPage(Pageable pageable);
}
