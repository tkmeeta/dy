package com.dy.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dy.base.dao.impl.BaseDaoImpl;
import com.dy.base.model.Page;
import com.dy.base.model.Pageable;
import com.dy.dao.IStuDao;
import com.dy.model.Stu;

@SuppressWarnings("rawtypes")
@Repository
@Transactional
public class StuDaoImpl extends BaseDaoImpl implements IStuDao {

	@Override
	public void saveStu(Stu stu) {
		String sql = "insert into stu(id,name) values (?,?)";
		this.update(sql, stu.getId(), stu.getName());
	}

	@Override
	public Stu queryStudent(int id) {
		String sql = "select id,name from stu where id = ?";
		return (Stu)this.find(sql, ParameterizedBeanPropertyRowMapper.newInstance(Stu.class), id);
	}
	
	@Override
	public List<Stu> queryStudents() {
		String sql = "select id,name from stu ";
		return this.getJdbcDao().getJdbcTemplate().query(sql, ParameterizedBeanPropertyRowMapper.newInstance(Stu.class));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<Stu> queryStuByPage(Pageable pageable) {
		String sql = "select id,name from stu where name like '%'||?||'%'";
		String countSql = "select count(1) from stu where name like '%'||?||'%'";
		Page<Stu> page = this.findPage(pageable, sql, countSql, ParameterizedBeanPropertyRowMapper.newInstance(Stu.class), "uu");
		return page;
	}

}
