package com.dy.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dy.base.dao.impl.BaseDaoImpl;
import com.dy.base.model.Page;
import com.dy.base.model.Pageable;
import com.dy.dao.IUserDao;
import com.dy.model.User;

@SuppressWarnings("rawtypes")
@Repository
@Transactional(readOnly=true)
public class UserDaoImpl extends BaseDaoImpl implements IUserDao{
	@Value("${datasource.sz}")
	private String DB_SZ;
	
	@Override
	public User queryUser(int id) {
		String sql = "select name from by_user where id=?";
		return (User)this.find(sql, ParameterizedBeanPropertyRowMapper.newInstance(User.class), DB_SZ, id);
	}

	@Override
	@Transactional(readOnly=false)
	public void saveUser(User user) {
		String sql = "insert into by_user(id,name) values (?,?)";
		this.update(sql, DB_SZ, user.getId(), user.getName());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> queryUsers() {
		String sql = "select id,name from by_user ";
		return this.list(sql, ParameterizedBeanPropertyRowMapper.newInstance(User.class), DB_SZ);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<User> queryUsersByPage(Pageable pageable) {
		String sql = "select id,name from by_user where name like '%'||?||'%'";
		String countSql = "select count(1) from by_user where name like '%'||?||'%'";
		Page<User> page = this.findPage(pageable, sql, countSql, DB_SZ, ParameterizedBeanPropertyRowMapper.newInstance(User.class), "ss");
		return page;
	}
}

