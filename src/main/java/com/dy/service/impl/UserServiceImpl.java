package com.dy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dy.base.model.Page;
import com.dy.base.model.Pageable;
import com.dy.dao.IUserDao;
import com.dy.model.User;
import com.dy.service.IUserService;

@Service(value="userService")
public class UserServiceImpl implements IUserService{

	@Autowired
	private IUserDao userDao;
	
	@Override
	public void saveUser(User user) {
		userDao.saveUser(user);
	}
	
	@Override
	public List<User> queryUsers() {
		List<User> list = userDao.queryUsers();
		return list;
	}
	
	@Override
	public Page<User> queryPageStus(Pageable pageable) {
		Page<User> list2 = userDao.queryUsersByPage(pageable);
		return list2;
	}
	
}
