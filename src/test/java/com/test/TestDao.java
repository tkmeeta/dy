package com.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.dy.dao.IStuDao;
import com.dy.dao.IUserDao;
import com.dy.model.Stu;
import com.dy.model.User;

@TransactionConfiguration(defaultRollback=false)
public class TestDao extends BaseContextTest{
	@Autowired
	private IStuDao stuDao;
	
	//@Test
	public void testStu(){
		Stu stu = new Stu("u1");
		stuDao.saveStu(stu);
	}
	
	@Autowired
	private IUserDao userDao;
	
	//@Test
	public void testUser(){
		User user = new User("user2");
		userDao.saveUser(user);
	}
}
