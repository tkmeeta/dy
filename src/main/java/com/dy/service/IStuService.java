package com.dy.service;

import java.util.List;

import com.dy.base.model.Page;
import com.dy.base.model.Pageable;
import com.dy.model.Stu;

public interface IStuService {
	public void saveStu(Stu stu);
	public Stu queryStu(int id);
	public List<Stu> queryStus();
	public Page<Stu> queryPageStus(Pageable pageable);
}
